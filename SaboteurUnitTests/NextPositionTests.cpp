#include "stdafx.h"
#include "CppUnitTest.h"
#include "../NextPosition.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurUnitTests
{
	TEST_CLASS(NextPositionTests)
	{
	public:

		TEST_METHOD(ConstructorWith4Parameters)
		{
			NextPosition nextPositionTest(NextPosition::RoadState::NotSpecified, NextPosition::RoadState::Opened, NextPosition::RoadState::Opened, NextPosition::RoadState::NotSpecified);
			Assert::IsTrue(nextPositionTest.getUpState() == NextPosition::RoadState::NotSpecified &&
				nextPositionTest.getDownState() == NextPosition::RoadState::Opened &&
				nextPositionTest.getLeftState() == NextPosition::RoadState::Opened &&
				nextPositionTest.getRightState() == NextPosition::RoadState::NotSpecified);
		}
	};
}