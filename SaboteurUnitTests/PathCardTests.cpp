#include "stdafx.h"
#include "CppUnitTest.h"
#include "../PathCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurUnitTests
{
	TEST_CLASS(PathCardTests)
	{
	public:

		TEST_METHOD(ConstructorWith4Parameters)
		{
			PathCard pathTest(PathCard::RoadState::Opened, PathCard::RoadState::Closed, PathCard::RoadState::None, PathCard::RoadState::Opened);
			Assert::IsTrue(pathTest.getUpState() == PathCard::RoadState::Opened &&
				pathTest.getDownState() == PathCard::RoadState::Closed &&
				pathTest.getLeftState() == PathCard::RoadState::None &&
				pathTest.getRightState() == PathCard::RoadState::Opened);
		}
		TEST_METHOD(ConstructorWithNoParameters)
		{
			PathCard pathTest;
			if (pathTest.getUpState() == PathCard::RoadState::None &&
				pathTest.getDownState() == PathCard::RoadState::None &&
				pathTest.getRightState() == PathCard::RoadState::None &&
				pathTest.getLeftState() == PathCard::RoadState::None)
			{
			}
			else
			{
				Assert::Fail();
			}

		}
		TEST_METHOD(RotateCardFuntion)
		{
			PathCard pathTest(PathCard::RoadState::Opened, PathCard::RoadState::Closed, PathCard::RoadState::None, PathCard::RoadState::Opened);
			pathTest.rotate();
			Assert::IsTrue(pathTest.getUpState() == PathCard::RoadState::Closed &&
				pathTest.getDownState() == PathCard::RoadState::Opened &&
				pathTest.getLeftState() == PathCard::RoadState::Opened &&
				pathTest.getRightState() == PathCard::RoadState::None);
		}
		TEST_METHOD(CopyCardFunction)
		{
			PathCard pathTest;
			PathCard pCard2(pathTest);
			if (pathTest.getUpState() == PathCard::RoadState::None &&
				pathTest.getDownState() == PathCard::RoadState::None &&
				pathTest.getRightState() == PathCard::RoadState::None &&
				pathTest.getLeftState() == PathCard::RoadState::None)
			{
			}
			else
			{
				Assert::Fail();
			}


		}
	
	};
}