#include "stdafx.h"
#include "CppUnitTest.h"
#include "../DwarfCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurUnitTests
{
	TEST_CLASS(DwarfCardTests)
	{
	public:
		TEST_METHOD(ConstructorWithParameter)
		{
			DwarfCard cardTest(DwarfCard::DwarfType::DiggerDwarf);
			Assert::IsTrue(cardTest.getDwarfType() == DwarfCard::DwarfType::DiggerDwarf);
		}
		TEST_METHOD(DwarfCardCopy)
		{
			DwarfCard cardTest(DwarfCard::DwarfType::DiggerDwarf);;
			DwarfCard copyTest(cardTest);
			Assert::IsTrue(copyTest.getDwarfType() == DwarfCard::DwarfType::DiggerDwarf);
		}
	};
}