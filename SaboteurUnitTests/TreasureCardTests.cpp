#include "stdafx.h"
#include "CppUnitTest.h"
#include "../TreasureCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurUnitTests
{
	TEST_CLASS(TreasureCardTests)
	{
	public:
		TEST_METHOD(ConstructorWithParameter)
		{
			TreasureCard cardTest(TreasureCard::TreasureType::GoldType);
			Assert::IsTrue(cardTest.getTreasureType() == TreasureCard::TreasureType::GoldType);
		}
		TEST_METHOD(DwarfCardCopy)
		{
			TreasureCard cardTest(TreasureCard::TreasureType::GoldType);;
			TreasureCard copyTest(cardTest);
			Assert::IsTrue(copyTest.getTreasureType() == TreasureCard::TreasureType::GoldType);
		}
	};
}