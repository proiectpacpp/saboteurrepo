#include "stdafx.h"
#include "CppUnitTest.h"
#include "../ActionCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurUnitTests
{
	TEST_CLASS(ActionCardTests)
	{
	public:

		TEST_METHOD(ConstructorWithNoParameters)
		{
			ActionCard cardTest;
			Assert::IsTrue(cardTest.getActionType() == ActionCard::ActionType::None);
		}
		TEST_METHOD(ConstructorWithParameter)
		{
			ActionCard cardTest(ActionCard::ActionType::deletePath);
			Assert::IsTrue(cardTest.getActionType() == ActionCard::ActionType::deletePath);
		}
		TEST_METHOD(ActionCardCopy)
		{
			ActionCard cardTest(ActionCard::ActionType::deletePath);;
			ActionCard copyTest(cardTest);
			Assert::IsTrue(copyTest.getActionType() == ActionCard::ActionType::deletePath);
		}


	};
}