#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Card.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurUnitTests
{
	TEST_CLASS(CardTests)
	{
		TEST_METHOD(CardType)
		{
			Card cardTest(Card::CardType::ActionCard);
			Assert::IsTrue(cardTest.getCardType() == Card::CardType::ActionCard);
		}
	};
}