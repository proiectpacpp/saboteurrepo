#pragma once
#include "Card.h"

#include <utility>
#include <iostream>

class PathCard :
	public Card
{
public:
	enum class RoadState{
		None,
		Opened,
		Closed
	};
public:
	//constructors
	PathCard();
	PathCard(RoadState up, RoadState down, RoadState left, RoadState right);
	PathCard(const PathCard& other);
	PathCard(PathCard&& other);
	PathCard(const std::string& card);
	//assignment operators
	PathCard& operator = (const PathCard& other);
	PathCard& operator = (PathCard&& other);
	//getters
	RoadState getUpState() const;
	RoadState getDownState() const;
	RoadState getLeftState() const;
	RoadState getRightState() const;
	bool getState() const;

	//Setters for state
	//-----------------
	void setActive();
	void setInactive();
	//-----------------


	void rotate();

	friend std::ostream& operator << (std::ostream& os, const PathCard& card);

public:
	std::string toString();
private:
	RoadState _up;
	RoadState _down;
	RoadState _left;
	RoadState _right;

	bool _isActive;
};

