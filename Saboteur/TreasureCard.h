#pragma once
#include "Card.h"
#include <iostream>

class TreasureCard :
	public Card
{
public:
	enum class TreasureType {
		GoldType,
		CoalType
	};
public:
	// constructors
	TreasureCard(TreasureType treasureType);
	TreasureCard(const TreasureCard& other);
	TreasureCard(TreasureCard&& other);
	TreasureCard();

	void makeItGolden();

	// operators
	TreasureCard& operator = (const TreasureCard& other);
	TreasureCard& operator = (TreasureCard&& other);

	friend std::ostream& operator<<(std::ostream & os, const TreasureCard & card);
public:
	TreasureCard::TreasureType getTreasureType() const;
private:
	TreasureType _treasureType;
};

