#pragma once
#include "PathCard.h"

#include <array>

class NextPosition
{
public:
	enum class RoadState {
		Opened,
		Closed,
		NotSpecified
	};

public:
	//I still don't know why, but the compiler say that I need a default constructor
	NextPosition();
	//-----
	NextPosition(const RoadState& up,const RoadState& down,const RoadState& left,const RoadState& right);
	void AddOpening(const int pos);
	void CloseRoad(const int pos);
	void RemoveConstraint(const int pos);
	//getters
	RoadState getUpState() const;
	RoadState getDownState() const;
	RoadState getLeftState() const;
	RoadState getRightState() const;

	bool cardFit(const PathCard& card);


public:
	bool getState() const;

private:
	RoadState& intToPath(const int path);

	bool equalStates(const PathCard::RoadState& roadCard, const RoadState& roadPosition);

private:
	RoadState _up;
	RoadState _down;
	RoadState _left;
	RoadState _right;

	bool _isActive;
};

