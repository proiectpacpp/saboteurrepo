#include "Player.h"
#include <string>
Card * Player::getCardByIndex(const int id)
{
	if (id < 0 || id >= _hand.size()) {
		throw "The index does not match";
	}

	return _hand[id];
}
void Player::eraseCard(const int id)
{
	_hand.erase(_hand.begin() + id);
}
Player::Player(const std::string & name, const DwarfCard& role) :
	_name(name),
	_cart(Player::Cart::Cart),
	_pickaxe(Player::Pickaxe::Pickaxe),
	_light(Player::Light::Light),
	_role(role)
{
	//empty
}
const std::string & Player::getName() const
{
	return _name;
}
void Player::cartDamage()
{
	_cart = Cart::CartBroken;
}
void Player::lightDamage()
{
		_light = Light::LightBroken;
}
void Player::pickaxeDamage()
{
	_pickaxe = Pickaxe::PickaxeBroken;
}
void Player::cartRepair()
{
	_cart = Cart::Cart;
}
void Player::lightRepair()
{
	_light = Light::Light;
}
void Player::pickaxeRepair()
{
	_pickaxe = Pickaxe::Pickaxe;
}
bool Player::getOperational()
{
	return getLightStatus() && getCartStatus() && getPickaxeStatus();
}
bool Player::getLightStatus()
{
	return _light == Light::Light;
}
bool Player::getPickaxeStatus()
{
	return _pickaxe==Pickaxe::Pickaxe;
}
bool Player::getCartStatus()
{
	return _cart == Cart::Cart;
}
bool Player::getPlayerState()
{
	return _isPlaying;
}
DwarfCard  Player::getRole() const
{
	return _role;
}
void Player::newCard(Card * card)
{
	_hand.push_back(card);
}
void Player::discardCard(const int index)
{
	if (index >= _hand.size()) {
		throw "The index you entered does not match";
	}
	_hand.erase(_hand.begin() + index);
}
void Player::discardAll()
{
	_isPlaying = false;
}

std::ostream & operator<<(std::ostream & os, const Player & player)
{
	os << player._name << " ";
	if (player.getRole().getDwarfType() == DwarfCard::DwarfType::DiggerDwarf) {
		os << "Digger";
	}
	else {
		os << "Saboteur";
	}
	os << std::endl << "Your status:" << std::endl;
	if (player._cart == Player::Cart::Cart) {
		os << "Cart functionally ";
	}
	else {
		os << "Cart Broken ";
	}
	if (player._light == Player::Light::Light) {
		os << "Light functionally ";
	}
	else {
		os << "Light broken ";
	}
	if (player._pickaxe == Player::Pickaxe::Pickaxe) {
		os << "Pickaxe functionally ";
	}
	else {
		std::cout << "Pickaxe broken ";
	}

	os << std::endl << "Your cards:" << std::endl;
	int index = 0;
	std::for_each(player._hand.begin(), player._hand.end(), [&os,&index](Card* card) {

		if (card->getCardType() == Card::CardType::PathCard) {
			os<<index<<" " << *static_cast<PathCard*>(card) ;
			index++;
		}
		if (card->getCardType() == Card::CardType::ActionCard) {
			os <<index<<" "<< *static_cast<ActionCard*>(card) ;
			index++;
		}

	});
	return os;
}
