#pragma once
#include <array>
#include "ActionCard.h"
class ActionCardsDeck
{
public:
	ActionCardsDeck();
	~ActionCardsDeck();
	std::array<ActionCard, 27>& getDeck();
private:
	//Hard-codded ActionCards array
	std::array<ActionCard, 27> ActionDeck;
	//The number of each type of card (Rules)
	int NumberOfSeeTreasureCards = 6;
	int NumberOfDeletePathCards = 3;
	int NumberOfRepairCartCards = 2;
	int NumberOfRepairLightCards = 2;
	int NumberOfRepairPickAxeCards = 2;
	int NumberOfRepairCartOrPickaxeCards = 1;
	int NumberOfDestroyCartCards = 3;
	int NumberOfDestoryLightCards = 3;
	int NumberOfDestoryPickAxeCards = 3;
	int NumberOfRepairCartOrLightCards = 1;
	int NumberOfRepairPickAxeOrLight = 1;
	int size = 0;
};

