#include "TreasureCard.h"

TreasureCard::TreasureCard(TreasureType treasureType):
	Card(Card::CardType::TreasureCard),
	_treasureType(treasureType)
{
}

TreasureCard::TreasureType TreasureCard::getTreasureType() const
{
	return _treasureType;
}

TreasureCard::TreasureCard(const TreasureCard & other) :
	Card(Card::CardType::TreasureCard)
{
	*this = other;
}

TreasureCard::TreasureCard(TreasureCard && other) :
	Card(Card::CardType::TreasureCard)
{
	*this = std::move(other);
}

TreasureCard::TreasureCard() :
	Card(Card::CardType::TreasureCard),
	_treasureType(TreasureCard::TreasureType::CoalType)
{
}

void TreasureCard::makeItGolden()
{
	_treasureType = TreasureCard::TreasureType::GoldType;
}



TreasureCard & TreasureCard::operator=(const TreasureCard & other)
{
	_treasureType = other._treasureType;
	return *this;
}

TreasureCard & TreasureCard::operator=(TreasureCard && other)
{
	*this = other;
	new(&other) Card(Card::CardType::TreasureCard);
	return *this;
}

std::ostream & operator<<(std::ostream & os, const TreasureCard & card)
{
	switch (card.getTreasureType())
	{
	case TreasureCard::TreasureType::CoalType:
		os << "Coal Card" << std::endl;
		break;
	case TreasureCard::TreasureType::GoldType:
		os << "Gold Card" << std::endl;
		break;
	}

	return os;
}
