#include "Card.h"

Card::Card(CardType cardType):
	_cardType(cardType)
{
}

Card::CardType Card::getCardType() const
{
	return _cardType;
}
