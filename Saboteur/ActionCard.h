#pragma once
#include "Card.h"
#include <iostream>
class ActionCard :
	public Card
{
public:
	enum class ActionType {
		destroyCart,
		destoryLight,
		destoryPickAxe,
		repairCart,
		repairLight,
		repairPickAxe,
		repairCartOrPickAxe,
		repairCartOrLight,
		repairPickAxeOrLight,
		deletePath,
		seeTreasure,
		None
	};
public:
	ActionCard(ActionType actionType);
	ActionCard(const ActionCard &other);
	ActionCard(ActionCard && other);
	ActionCard();
	ActionCard & operator = (const ActionCard & other);
	ActionCard & operator = ( ActionCard && other);

	friend std::ostream& operator << (std::ostream& os, const ActionCard &card);

	ActionCard::ActionType getActionType() const;

	bool repairOrDestroyOneThing();
	bool repairTwoThings();


private:
	ActionType _actionType;
};

