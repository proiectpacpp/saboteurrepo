#include "Game.h"
#include <algorithm>
#include <random>
Game::Game()
{
}


Game::~Game()
{
}

void Game::PushingDwarfs(const int & numberofDiggers, const  int& numberOfSaboteurs)
{
	for (int i = 0; i < numberofDiggers; i++)
		DwarfVect.push_back(DwarfCard::DwarfType::DiggerDwarf);
	for (int i = 0; i < numberOfSaboteurs; i++)
		DwarfVect.push_back(DwarfCard::DwarfType::SaboteurDwarf);

}

std::vector<DwarfCard>& Game::getCards()
{
	return DwarfVect;
}

void Game::initializeGame(const int & numberOfPlayers)
{
	if(numberOfPlayers>10)
	{
		throw "The maximum number of players is 10";
	}

	if (numberOfPlayers == 3)
	{
		PushingDwarfs(3,1);
	}
	if(numberOfPlayers == 4)
	{
		PushingDwarfs(4, 1);
	}
	if (numberOfPlayers == 5)
	{
		PushingDwarfs(4, 2);
	}
	if (numberOfPlayers == 6)
	{
		PushingDwarfs(5, 2);
	}
	if (numberOfPlayers == 7)
	{
		PushingDwarfs(5, 3);
	}
	if (numberOfPlayers == 8)
	{
		PushingDwarfs(6, 3);
	}
	if (numberOfPlayers == 9)
	{
		PushingDwarfs(7, 3);
	}
	if (numberOfPlayers == 10)
	{
		PushingDwarfs(7, 4);
	}
	std::default_random_engine randomEngine;
	std::shuffle(DwarfVect.begin(), DwarfVect.end(), randomEngine);
}
