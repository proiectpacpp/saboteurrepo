#pragma once


class Card
{
public:
	enum class CardType {
		PathCard,
		ActionCard,
		DwarfCard,
		TreasureCard
	};
public:
	Card(CardType cardType);
public:
	CardType getCardType() const ;
private:
	CardType _cardType;

};

