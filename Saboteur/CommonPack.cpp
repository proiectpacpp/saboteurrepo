#include "CommonPack.h"
#include <vector>
#include <random>

CommonPack::CommonPack()
{
	PathCardsDeck paths;
	ActionCardsDeck actions;

	std::vector<Card*> aux;

	std::for_each(paths.getCards().begin(), paths.getCards().end(), [&aux](PathCard& card) {
		aux.push_back(&card);
	});

	std::for_each(actions.getDeck().begin(), actions.getDeck().end(), [&aux](ActionCard& card) {
		aux.push_back(&card);
	});
	
	std::default_random_engine randomEngine;
	std::shuffle(aux.begin(), aux.end(),randomEngine );

	for (auto& it : aux) {
		
		_cards.push(it);

	}

	
}

Card * CommonPack::get()
{
	if (_cards.empty()) {
		throw "The deck is empty";
	}

	Card* aux = _cards.top();
	_cards.pop();
	return aux;

}

