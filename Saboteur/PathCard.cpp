#include "PathCard.h"

PathCard::PathCard():
	Card(Card::CardType::PathCard)
{
	_up = RoadState::None;
	_down = RoadState::None;
	_left = RoadState::None;
	_right = RoadState::None;
	_isActive = false;
}

PathCard::PathCard(RoadState up, RoadState down, RoadState left, RoadState right):
	Card(Card::CardType::PathCard),
	_left(left),
	_right(right),
	_down(down),
	_up(up),
	_isActive(true)
{
	//empty
}

PathCard::PathCard(const PathCard & other):
	Card(Card::CardType::PathCard)
{
	*this = other;
}

PathCard::PathCard(PathCard && other):
	Card(Card::CardType::PathCard)
{
	*this = std::move(other);
}

PathCard::PathCard(const std::string & card):
	Card(Card::CardType::PathCard)
{
	switch (card[0])
	{
	case '0':
		_up = RoadState::None;
		break;
	case '1':
		_up = RoadState::Closed;
		break;
	case'2':
		_up = RoadState::Opened;
		break;
	}
	switch (card[1])
	{
	case '0':
		_down = RoadState::None;
		break;
	case '1':
		_down = RoadState::Closed;
		break;
	case'2':
		_down = RoadState::Opened;
		break;
	}
	switch (card[2])
	{
	case '0':
		_right = RoadState::None;
		break;
	case '1':
		_right = RoadState::Closed;
		break;
	case'2':
		_right = RoadState::Opened;
		break;
	}
	switch (card[3])
	{
	case '0':
		_left = RoadState::None;
		break;
	case '1':
		_left = RoadState::Closed;
		break;
	case'2':
		_left = RoadState::Opened;
		break;
	}
}

PathCard & PathCard::operator=(const PathCard & other)
{
	_down = other._down;
	_up = other._up;
	_left = other._left;
	_right = other._right;
	_isActive = other._isActive;
	return *this;
}

PathCard & PathCard::operator=(PathCard && other)
{
	*this = other;
	new(&other) PathCard;
	return *this;
}

PathCard::RoadState PathCard::getUpState() const
{
	return _up;
}

PathCard::RoadState PathCard::getDownState() const
{
	return _down;
}

PathCard::RoadState PathCard::getLeftState() const
{
	return _left;
}

PathCard::RoadState PathCard::getRightState() const
{
	return _right;
}

bool PathCard::getState() const
{
	return _isActive;
}

void PathCard::setActive()
{
	_isActive = true;
}

void PathCard::setInactive()
{
	_isActive = false;
}

void PathCard::rotate()
{
	std::swap(_left, _right);
	std::swap(_up, _down);
}

std::string PathCard::toString()
{
	std::string result;
	switch (_up)
	{
	case PathCard::RoadState::None:
		result.push_back('0');
		break;
	case PathCard::RoadState::Opened:
		result.push_back('2');
		break;
	case PathCard::RoadState::Closed:
		result.push_back('1');
		break;
	default:
		break;
	}
	switch (_down)
	{
	case PathCard::RoadState::None:
		result.push_back('0');
		break;
	case PathCard::RoadState::Opened:
		result.push_back('2');
		break;
	case PathCard::RoadState::Closed:
		result.push_back('1');
		break;
	default:
		break;
	}
	switch (_right)
	{
	case PathCard::RoadState::None:
		result.push_back('0');
		break;
	case PathCard::RoadState::Opened:
		result.push_back('2');
		break;
	case PathCard::RoadState::Closed:
		result.push_back('1');
		break;
	default:
		break;
	}
	switch (_left)
	{
	case PathCard::RoadState::None:
		result.push_back('0');
		break;
	case PathCard::RoadState::Opened:
		result.push_back('2');
		break;
	case PathCard::RoadState::Closed:
		result.push_back('1');
		break;
	default:
		break;
	}
	return result;
}

std::ostream & operator<<(std::ostream & os, const PathCard & card)
{
	switch (card._up)
	{
	case PathCard::RoadState::Opened:
		os << "up: opened ";
		break;
	case PathCard::RoadState::Closed:
		os << "up: closed " ;
		break;
	default:
		os << "up: None ";
	}

	switch (card._down)
	{
	case PathCard::RoadState::Opened:
		os << "down: opened ";
		break;
	case PathCard::RoadState::Closed:
		os << "down: closed ";
		break;
	default:
		os << "down: None ";
	}

	switch (card._left)
	{
	case PathCard::RoadState::Opened:
		os << "left: opened ";
		break;
	case PathCard::RoadState::Closed:
		os << "left: closed ";
		break;
	default:
		os << "left: None ";
	}

	switch (card._right)
	{
	case PathCard::RoadState::Opened:
		os << "right: opened ";
		break;
	case PathCard::RoadState::Closed:
		os << "right: closed ";
		break;
	default:
		os << "right: None ";
	}
	os << std::endl;
	return os;
}
