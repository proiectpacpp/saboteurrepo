#pragma once
#include "PathCard.h"
#include "ActionCard.h"
#include "Card.h"
#include "PathCardsDeck.h"
#include "ActionCardsDeck.h"

#include <stack>
#include <memory>

class CommonPack
{
public:
	CommonPack();
	Card* get();

private:
	std::stack<Card * > _cards;
};

