#include "ActionCard.h"
#include <iostream>
ActionCard::ActionCard(ActionType actionType) :
	Card(Card::CardType::ActionCard),
	_actionType(actionType)
{
}
ActionCard::ActionType ActionCard::getActionType() const
{
	return _actionType;
}
bool ActionCard::repairOrDestroyOneThing()
{
	return (_actionType != ActionType::deletePath && _actionType != ActionType::repairCartOrLight
		&& _actionType != ActionType::repairCartOrPickAxe && _actionType != ActionType::repairPickAxeOrLight
		&& _actionType != ActionType::seeTreasure
		);
}
bool ActionCard::repairTwoThings()
{
	return (_actionType==ActionType::repairCartOrLight || _actionType==ActionType::repairCartOrPickAxe || _actionType==ActionType::repairPickAxeOrLight);
}
ActionCard::ActionCard(const ActionCard & other) :
	Card(Card::CardType::ActionCard)
{
	*this = other;
}
ActionCard::ActionCard(ActionCard && other) :
	Card(Card::CardType::ActionCard)
{
	*this = std::move(other);
}

ActionCard::ActionCard() :
	Card(Card::CardType::ActionCard)
{
	_actionType = ActionType::None;
}

ActionCard & ActionCard::operator=(const ActionCard & other)
{
	_actionType = other._actionType;

	return *this;

}

ActionCard & ActionCard::operator=(ActionCard && other)
{
	_actionType = other._actionType;

	new(&other) ActionCard;

	return *this;

}

std::ostream & operator<<(std::ostream & os, const ActionCard &card)
{
	switch (card.getActionType())
	{
	case ActionCard::ActionType::deletePath:
		os << "Delete a path" << std::endl;
		break;
	case ActionCard::ActionType::destoryLight:
		os << "Destroy Light" << std::endl;
		break;
	case ActionCard::ActionType::destoryPickAxe:
		os << "Destroy PickAxe" << std::endl;
		break;
	case ActionCard::ActionType::destroyCart:
		os << "Destroy Cart" << std::endl;
		break;
	case ActionCard::ActionType::repairCart:
		os << "Repair a Cart" << std::endl;
		break;
	case ActionCard::ActionType::repairCartOrLight:
		os << "Repair a Cart or Light" << std::endl;
		break;
	case ActionCard::ActionType::repairCartOrPickAxe:
		os << "Repair a Cart or PickAxe" << std::endl;
		break;
	case ActionCard::ActionType::repairLight:
		os << "Repair Light" << std::endl;
		break;
	case ActionCard::ActionType::repairPickAxe:
		os << "Repair PickAxe" << std::endl;
		break;
	case ActionCard::ActionType::repairPickAxeOrLight:
		os << "Repair a PickAxe or Light" << std::endl;
		break;
	case ActionCard::ActionType::seeTreasure:
		os << "See a Treasure Card" << std::endl;
		break;
	case ActionCard::ActionType::None:
		os << "None" << std::endl;
		break;
	}


	return os;
}


