#include "PathCardsDeck.h"



PathCardsDeck::PathCardsDeck()
{
	//hardcoded
	int addedCards = 0;

	for (int i = 0; i < 4; i++) {
		cards[addedCards++] = PathCard(PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::None, PathCard::RoadState::None);
	}
	for (int i = 0; i < 5; i++) {
		cards[addedCards++] = PathCard(PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::None, PathCard::RoadState::Opened);
	}

	for (int i = 0; i < 5; i++) {
		cards[addedCards++] = PathCard(PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened);
	}
	for (int i = 0; i < 4; i++) {
		cards[addedCards++] = PathCard(PathCard::RoadState::None, PathCard::RoadState::Opened, PathCard::RoadState::None, PathCard::RoadState::Opened);
	}
	for (int i = 0; i < 5; i++) {
		cards[addedCards++] = PathCard(PathCard::RoadState::None, PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::None);
	}
	cards[addedCards++] = PathCard(PathCard::RoadState::None, PathCard::RoadState::Closed, PathCard::RoadState::None, PathCard::RoadState::None);
	cards[addedCards++] = PathCard(PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::None);
	cards[addedCards++] = PathCard(PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::Closed);
	cards[addedCards++] = PathCard(PathCard::RoadState::None, PathCard::RoadState::Closed, PathCard::RoadState::None, PathCard::RoadState::Closed);
	cards[addedCards++] = PathCard(PathCard::RoadState::None, PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::None);
	cards[addedCards++] = PathCard(PathCard::RoadState::None, PathCard::RoadState::None, PathCard::RoadState::Closed, PathCard::RoadState::None);
	cards[addedCards++] = PathCard(PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::None, PathCard::RoadState::None);
	cards[addedCards++] = PathCard(PathCard::RoadState::Closed, PathCard::RoadState::None, PathCard::RoadState::Closed, PathCard::RoadState::Closed);
	cards[addedCards++] = PathCard(PathCard::RoadState::None, PathCard::RoadState::None, PathCard::RoadState::Closed, PathCard::RoadState::Closed);


	for (int i = 0; i < 5; i++) {
		cards[addedCards++] = PathCard(PathCard::RoadState::Opened, PathCard::RoadState::None, PathCard::RoadState::Opened, PathCard::RoadState::Opened);
	}

	for (int i = 0; i < 3; i++) {
		cards[addedCards++] = PathCard(PathCard::RoadState::None, PathCard::RoadState::None, PathCard::RoadState::Opened, PathCard::RoadState::Opened);
	}

}

std::array<PathCard, 40>& PathCardsDeck::getCards()
{
	return cards;
}

