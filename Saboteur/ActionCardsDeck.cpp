#include "ActionCardsDeck.h"



ActionCardsDeck::ActionCardsDeck()
{
	for (int i = 0; i < NumberOfSeeTreasureCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::seeTreasure;
		size++;
	}
	for (int i = 0; i < NumberOfDeletePathCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::deletePath;
		size++;
	}
	for (int i = 0; i < NumberOfRepairCartCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::repairCart;
		size++;
	}
	for (int i = 0; i < NumberOfRepairLightCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::repairLight;
		size++;
	}
	for (int i = 0; i < NumberOfRepairPickAxeCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::repairPickAxe;
		size++;
	}
	for (int i = 0; i < NumberOfRepairCartOrPickaxeCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::repairCartOrPickAxe;
		size++;
	}
	for (int i = 0; i < NumberOfDestroyCartCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::destroyCart;
		size++;
	}
	for (int i = 0; i < NumberOfDestoryLightCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::destoryLight;
		size++;
	}
	for (int i = 0; i < NumberOfDestoryPickAxeCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::destoryPickAxe;
		size++;
	}
	for (int i = 0; i < NumberOfRepairCartOrLightCards; i++) {
		ActionDeck[size] = ActionCard::ActionType::repairCartOrLight;
		size++;
	}
	for (int i = 0; i < NumberOfRepairPickAxeOrLight; i++) {
		ActionDeck[size] = ActionCard::ActionType::repairPickAxeOrLight;
		size++;
	}
}


ActionCardsDeck::~ActionCardsDeck()
{
}

std::array<ActionCard, 27>& ActionCardsDeck::getDeck()
{
	return this->ActionDeck;
}