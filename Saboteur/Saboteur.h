#pragma once
#include "Card.h"
#include "DwarfCard.h"
#include "PathCard.h"
#include "Board.h"
#include "Player.h"
#include "ActionCard.h"

#include <vector>
#include <iostream>
#include <stack>
#include <array>

class Saboteur
{
public:
	Saboteur();
	void initPlayers();

	void startGame();

private:

	void generateDwards(const int number, std::vector<DwarfCard>& aux);
	void pushingDwarfs(const int numberofDiggers, const int numberOfSaboteurs,std::vector<DwarfCard>& container);


private:
	
	std::stack<Card*> _deck;
	std::vector<Player> _players ;
	Board _board;
	//don't
	std::vector<Card*> aux;
};

