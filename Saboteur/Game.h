#pragma once
#include <vector>
#include "DwarfCard.h"
class Game
{
public:
	Game();
	~Game();
	void initializeGame(const int &numberOfPlayers);
	std::vector<DwarfCard> DwarfVect;
	std::vector<DwarfCard> &getCards();
private:
	void PushingDwarfs(const int & numberofDiggers, const int& numberOfSaboteurs);
};

