#pragma once
#include "Card.h"
#include <iostream>

class DwarfCard 
	
{
public:
	enum class DwarfType {
		DiggerDwarf,
		SaboteurDwarf
	};
public:
	
	DwarfCard(DwarfType dwarfType);
	DwarfCard(const DwarfCard& other);
	DwarfCard(DwarfCard&& other);

	DwarfCard& operator=(const DwarfCard& other);
	DwarfCard& operator=(DwarfCard&& other);
	friend std::ostream& operator<<(std::ostream & os, const DwarfCard & card);
public:
	DwarfCard::DwarfType getDwarfType() const;
private:
	DwarfType _dwarfType;
};

