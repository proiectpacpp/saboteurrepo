#pragma once
#include "Board.h"
#include "Card.h"
#include "ActionCardsDeck.h"
#include "PathCardsDeck.h"
#include "DwarfCard.h"
class Player
{
public:
	enum class Cart {
		CartBroken,
		Cart
	};
	enum class Light {
		Light,
		LightBroken
	};
	enum class Pickaxe {
		PickaxeBroken,
		Pickaxe
	};



public:

	Card* getCardByIndex(const int id);
	void eraseCard(const int id);

	Player(const std::string & name, const DwarfCard& role);

	const std::string& getName() const;

	void cartDamage();
	void lightDamage();
	void pickaxeDamage();
	void cartRepair();
	void lightRepair();
	void pickaxeRepair();

	bool getOperational();
	bool getLightStatus();
	bool getPickaxeStatus();
	bool getCartStatus();

	bool getPlayerState();

	DwarfCard getRole() const;

	friend std::ostream& operator << (std::ostream& os, const Player& player);

public:

	void newCard(Card* card);
	void discardCard(const int index);
	void discardAll();

private:
	std::string _name;
	Cart _cart;
	Light _light;
	Pickaxe _pickaxe;
	bool _isPlaying = true;
	std::vector<Card*> _hand;
	DwarfCard _role;
};

