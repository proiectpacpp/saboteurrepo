#pragma once

#include "PathCard.h"

#include <array>

class PathCardsDeck
{
private:
	std::array<PathCard, 40> cards;
public:
	PathCardsDeck();

public:
	std::array<PathCard, 40>& getCards() ;
};

