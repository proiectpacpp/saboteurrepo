#include "Board.h"

#include <string>
#include <random>
#include <queue>

bool Board::PosComparator::operator()(const Position& pos1,const  Position& pos2) const
{
	if (pos1.first < pos2.first) {
		return true;
	}
	else {
		if (pos1.first == pos2.first) {
			if (pos1.second < pos2.second) {
				return true;
			}
		}
	}
	return false;
}

Board::Board()
{
	_cards[Position(0, 0)] = PathCard(RoadS::Opened, RoadS::Opened, RoadS::Opened, RoadS::Opened);
	_availablePositions[Position(1, 0)] = NextPosition(AvailableRoad::Opened,AvailableRoad::NotSpecified, AvailableRoad::NotSpecified, AvailableRoad::NotSpecified);
	_availablePositions[Position(0, 1)] =NextPosition(AvailableRoad::NotSpecified, AvailableRoad::NotSpecified, AvailableRoad::Opened, AvailableRoad::NotSpecified);
	_availablePositions[Position(-1, 0)] =NextPosition(AvailableRoad::NotSpecified, AvailableRoad::Opened, AvailableRoad::NotSpecified, AvailableRoad::NotSpecified);
	_availablePositions[Position(0, -1)] = NextPosition(AvailableRoad::NotSpecified, AvailableRoad::NotSpecified, AvailableRoad::NotSpecified, AvailableRoad::Opened);



	



	 int gold = rand()%3;
	_treasures[0] = TreasureCard(TreasureCard::TreasureType::CoalType);
	_treasures[1] = TreasureCard(TreasureCard::TreasureType::CoalType);
	_treasures[2] = TreasureCard(TreasureCard::TreasureType::CoalType);
	_treasures[gold].makeItGolden();
	_goldPos = _treasurePositions[gold];
	}

void Board::emplaceCard(const Position & pos,  PathCard & card)
{
	if (_availablePositions.find(pos) == _availablePositions.end()) {
		throw "The position you entered is not an available one";
	}

	if (!_availablePositions[pos].cardFit(card)) {
		throw "The card does not fit";
	}
	
	_availablePositions.erase(_availablePositions.find(pos));
	_cards[pos] = card;
	//adding available positions
	if(_cards.find(Position(pos.first, pos.second + 1)) == _cards.end() && std::find(_treasurePositions.begin(),_treasurePositions.end(),Position(pos.first,pos.second+1))==_treasurePositions.end())
		addPosition(pos, Position(pos.first, pos.second + 1));
	if (_cards.find(Position(pos.first, pos.second - 1)) == _cards.end() && std::find(_treasurePositions.begin(), _treasurePositions.end(), Position(pos.first, pos.second - 1)) == _treasurePositions.end())
		addPosition(pos, Position(pos.first, pos.second - 1));
	if (_cards.find(Position(pos.first + 1, pos.second )) == _cards.end() && std::find(_treasurePositions.begin(), _treasurePositions.end(), Position(pos.first +1 , pos.second )) == _treasurePositions.end())
		addPosition(pos, Position(pos.first +1, pos.second ));
	if (_cards.find(Position(pos.first - 1, pos.second)) == _cards.end() && std::find(_treasurePositions.begin(), _treasurePositions.end(), Position(pos.first -1 , pos.second )) == _treasurePositions.end())
		addPosition(pos, Position(pos.first -1, pos.second )); 


}

const std::string  Board::getTreasure(const int idx) const
{
	if (idx < 1 || idx > 3) {
		throw "The number you entered is not matching";
	}
	if (_treasures[idx - 1].getTreasureType() == TreasureCard::TreasureType::GoldType) {
		return "Gold";
	}
	return "Coal";
}

bool Board::check(const Position& lastAddedPos) const
{
	if (!neighbours(lastAddedPos, _goldPos)) {
		return false;
	}
	else {
		std::vector<Position> examined;
		std::queue<Position> queue;
		queue.push(lastAddedPos);
		while (!queue.empty()) {
			PathCard currentCard = _cards.at(queue.front());

			Position leftNeighbour = Position(queue.front().first  , queue.front().second -1);
			Position rightNeighbour = Position(queue.front().first , queue.front().second +1);
			Position upNeighbour = Position(queue.front().first -1 , queue.front().second );
			Position downNeighbour = Position(queue.front().first +1 , queue.front().second );
			
			if (_cards.find(leftNeighbour)!=_cards.end() 
				&& std::find(examined.begin(), examined.end(), leftNeighbour) == examined.end()) {
				if (currentCard.getLeftState() == PathCard::RoadState::Opened &&
					_cards.at(leftNeighbour).getRightState() == PathCard::RoadState::Opened) {
					if (leftNeighbour == Position(0, 0)) {
						return true;
					}
					queue.push(leftNeighbour);
				}
			}
			if (_cards.find(rightNeighbour) != _cards.end()
				&& std::find(examined.begin(), examined.end(), rightNeighbour) == examined.end()) {
				if (currentCard.getRightState() == PathCard::RoadState::Opened &&
					_cards.at(rightNeighbour).getLeftState() == PathCard::RoadState::Opened) {
					if (rightNeighbour == Position(0, 0)) {
						return true;
					}
					queue.push(rightNeighbour);
				}
			}
			if (_cards.find(upNeighbour) != _cards.end()
				&& std::find(examined.begin(), examined.end(), upNeighbour) == examined.end()) {
				if (currentCard.getUpState() == PathCard::RoadState::Opened &&
					_cards.at(upNeighbour).getDownState() == PathCard::RoadState::Opened) {
					if (upNeighbour == Position(0, 0)) {
						return true;
					}
					queue.push(upNeighbour);
				}
			}
			if (_cards.find(downNeighbour) != _cards.end()
				&& std::find(examined.begin(),examined.end(),downNeighbour) == examined.end()) {
				if (currentCard.getDownState() == PathCard::RoadState::Opened &&
					_cards.at(downNeighbour).getUpState() == PathCard::RoadState::Opened) {
					if (downNeighbour == Position(0, 0)) {
						return true;
					}
					queue.push(downNeighbour);
				}
			}
			examined.push_back(queue.front());
			queue.pop();
		}
	}
	return false;
}

void Board::deleteCard(const Position & pos)
{
	if (_cards.find(pos) == _cards.end()) {
		throw "There is no card on this position";
	}
	if (pos == Position(0, 0)) {
		throw "You cannot delete the start card";
	}
	_cards.erase(_cards.find(pos));
	if (_availablePositions.find(Position(pos.first + 1, pos.second)) != _availablePositions.end() && !hasCardsNeighbours(Position(pos.first+1,pos.second))) {
		_availablePositions.erase(_availablePositions.find(Position(pos.first + 1, pos.second)));
	}
	if (_availablePositions.find(Position(pos.first - 1, pos.second)) != _availablePositions.end() && !hasCardsNeighbours(Position(pos.first - 1, pos.second))) {
		_availablePositions.erase(_availablePositions.find(Position(pos.first - 1, pos.second)));
	}

	if (_availablePositions.find(Position(pos.first, pos.second + 1)) != _availablePositions.end() && !hasCardsNeighbours(Position(pos.first, pos.second + 1))) {
		_availablePositions.erase(_availablePositions.find(Position(pos.first, pos.second + 1)));
	}

	if (_availablePositions.find(Position(pos.first, pos.second - 1)) != _availablePositions.end() && !hasCardsNeighbours(Position(pos.first, pos.second - 1))) {
		_availablePositions.erase(_availablePositions.find(Position(pos.first, pos.second - 1)));
	}
	_availablePositions[pos] = NextPosition(AvailableRoad::NotSpecified, AvailableRoad::NotSpecified, AvailableRoad::NotSpecified, AvailableRoad::NotSpecified);
	if (_cards.find(Position(pos.first +1 , pos.second)) != _cards.end()) {
		if (_cards.at(Position(pos.first + 1, pos.second)).getUpState() != RoadS::None) {
			_availablePositions[pos].AddOpening(2);
		}
		else {
			_availablePositions[pos].CloseRoad(2);
		}
	}
	if (_cards.find(Position(pos.first - 1, pos.second)) != _cards.end()) {
		if (_cards.at(Position(pos.first - 1, pos.second)).getDownState() != RoadS::None) {
			_availablePositions[pos].AddOpening(1);
		}
		else {
			_availablePositions[pos].CloseRoad(1);
		}
	}

	if (_cards.find(Position(pos.first , pos.second + 1)) != _cards.end()) {
		if (_cards.at(Position(pos.first , pos.second + 1 )).getLeftState() != RoadS::None) {
			_availablePositions[pos].AddOpening(4);
		}
		else {
			_availablePositions[pos].CloseRoad(4);
		}
	}
	if (_cards.find(Position(pos.first, pos.second - 1)) != _cards.end()) {
		if (_cards.at(Position(pos.first, pos.second - 1)).getRightState() != RoadS::None) {
			_availablePositions[pos].AddOpening(3);
		}
		else {
			_availablePositions[pos].CloseRoad(3);
		}
	}
}

bool Board::hasCardsNeighbours(const Position & pos) const
{
	if (_cards.find(Position(pos.first + 1, pos.second)) != _cards.end()) {
		return true;
	}
	if (_cards.find(Position(pos.first - 1, pos.second)) != _cards.end()) {
		return true;
	}
	if (_cards.find(Position(pos.first , pos.second+1)) != _cards.end()) {
		return true;
	}
	if (_cards.find(Position(pos.first , pos.second-1)) != _cards.end()) {
		return true;
	}
	return false;
}

void Board::addPosition(const Position & cardPos, const Position & availablePos)
{
	//we create a position if there is no one :D
	if (_availablePositions.find(availablePos) == _availablePositions.end())
	{
		NextPosition aux = NextPosition(NextPosition::RoadState::NotSpecified, NextPosition::RoadState::NotSpecified, NextPosition::RoadState::NotSpecified, NextPosition::RoadState::NotSpecified);
		_availablePositions[availablePos] = aux;
	}
	if (cardPos.first - availablePos.first == 1) {
			//the available pos is on top of the card
			if (_cards[cardPos].getUpState() == RoadS::None) {
				_availablePositions[availablePos].CloseRoad(2);
			}
			else {
				_availablePositions[availablePos].AddOpening(2);
			}
			return;
	}
	
	if (cardPos.first - availablePos.first == -1) {
		//the available pos is under the card
		if (_cards[cardPos].getDownState() == RoadS::None) {
			_availablePositions[availablePos].CloseRoad(1);
		}
		else {
			_availablePositions[availablePos].AddOpening(1);
		}
		return;
	}
	
	if (cardPos.second - availablePos.second == 1) {
		//the available pos is in left of the card
		if (_cards[cardPos].getLeftState() == RoadS::None) {
			_availablePositions[availablePos].CloseRoad(4);
		}
		else {
			_availablePositions[availablePos].AddOpening(4);
		}
		return;
	}
	
	if (cardPos.second - availablePos.second == -1) {
		//the available pos is in right of the card
		if (_cards[cardPos].getRightState() == RoadS::None) {
			_availablePositions[availablePos].CloseRoad(3);
		}
		else {
			_availablePositions[availablePos].AddOpening(3);
		}
		return;
	}

}

bool Board::neighbours(const Position & pos1, const Position & pos2) const
{
	if (Position(pos1.first + 1, pos1.second) == pos2) {
		if(_cards.at(pos1).getDownState() == PathCard::RoadState::Opened)
			return true;
	}
	if (Position(pos1.first - 1, pos1.second) == pos2) {
		if (_cards.at(pos1).getUpState() == PathCard::RoadState::Opened)
		return true;
	}
	if (Position(pos1.first, pos1.second+1) == pos2) {
		if (_cards.at(pos1).getRightState() == PathCard::RoadState::Opened)
		return true;
	}
	if (Position(pos1.first , pos1.second-1) == pos2) {
		if (_cards.at(pos1).getLeftState() == PathCard::RoadState::Opened)
		return true;
	}
	return false;
}



std::ostream & operator<<(std::ostream & os, const Board & board)
{
	std::pair<int, int> upperLeft = std::make_pair(-2, 0);
	std::pair<int, int> bottomRight = std::make_pair(2, 8);
	std::for_each(board._availablePositions.begin(), board._availablePositions.end( ), [&bottomRight, & upperLeft](std::pair<Board::Position, NextPosition> elem) {
		if (elem.first.first > bottomRight.first) {
			bottomRight.first = elem.first.first;
		}
		
		if (elem.first.second > bottomRight.second) {
			bottomRight.second = elem.first.second;
		}
		
		if (elem.first.first < upperLeft.first) {
			 upperLeft.first = elem.first.first;
		}

		if (elem.first.second < upperLeft.second) {
			upperLeft.second = elem.first.second;
		}
	});
	//we will need 3 buffers
	//I will explain you how to use its

	std::string up;
	std::string middle;
	std::string down;

	for (int i = upperLeft.first; i <= bottomRight.first; i++) {
		for (int j = upperLeft.second; j <= bottomRight.second; j++) {
			
			if (std::any_of(board._treasurePositions.begin(), board._treasurePositions.end(), [i, j](Board::Position pos) {return pos == std::pair(i, j); }))
			{
				up = up +(char)219+ (char)219 + (char)219 ;
				down = down + (char)219 + (char)219 + (char)219;
				middle = middle + (char)219 + (char)219 + (char)219;
				
			}else
			if (board._cards.find(Board::Position(i, j)) != board._cards.end()) {
				//there will be a lot of magic numbers, I will solve this problem
				// ok will help us to establish if on the current card we have at least an opening
				bool ok = false;
				up.push_back((char)176);
				down.push_back((char)176);
				//emplace all ifs with a function

				if (board._cards.at(Board::Position(i, j)).getLeftState() == PathCard::RoadState::Opened) {
					middle.push_back('-');
					ok = true;
				}
				if (board._cards.at(Board::Position(i, j)).getLeftState() == PathCard::RoadState::Closed) {
					middle.push_back('#');
				}
				if (board._cards.at(Board::Position(i, j)).getLeftState() == PathCard::RoadState::None) {
					middle.push_back((char)176);
				}

				
				if (board._cards.at(Board::Position(i, j)).getUpState() == PathCard::RoadState::Opened) {
					up.push_back('|');
					ok = true;
				}
				if (board._cards.at(Board::Position(i, j)).getUpState() == PathCard::RoadState::Closed) {
					up.push_back('#');
				}
				if (board._cards.at(Board::Position(i, j)).getUpState() == PathCard::RoadState::None) {
					up.push_back((char)176);
				}

				if (board._cards.at(Board::Position(i, j)).getDownState() == PathCard::RoadState::Opened) {
					down.push_back('|');
					ok = true;
				}
				if (board._cards.at(Board::Position(i, j)).getDownState() == PathCard::RoadState::Closed) {
					down.push_back('#');
				}
				if (board._cards.at(Board::Position(i, j)).getDownState() == PathCard::RoadState::None) {
					down.push_back((char(176)));
				}
				
				if (ok) {
					if (i == 0 && j == 0)
						middle.push_back((char)206);
					else {
						middle.push_back('+');
					}
				}
				else {
					middle.push_back((char)176);
				}
				up.push_back((char)176);
				down.push_back((char)176);

				if (board._cards.at(Board::Position(i, j)).getRightState() == PathCard::RoadState::Opened) {
					middle.push_back('-');
					ok = true;
				}
				if (board._cards.at(Board::Position(i, j)).getRightState() == PathCard::RoadState::Closed) {
					middle.push_back('#');
				}
				if (board._cards.at(Board::Position(i, j)).getRightState() == PathCard::RoadState::None) {
					middle.push_back((char)176);
				}

			}
			else {
				up = up + "   ";
				down = down + "   ";
				if (board._availablePositions.find(Board::Position(i, j)) != board._availablePositions.end()) {
					middle = middle + " " + (char)254 + " ";
				}
				else {
					middle = middle + "   ";
				}
			}
			
		}
		//here we will print the buffers and clear them
		os << up << std::endl;
		os << middle << std::endl;
		os << down << std::endl;
		up.clear();
		middle.clear();
		down.clear();
	}
	return os;
}
