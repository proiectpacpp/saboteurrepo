#include "NextPosition.h"

NextPosition::NextPosition()
{
}

NextPosition::NextPosition(const RoadState& up = RoadState::NotSpecified,const RoadState& down = RoadState::NotSpecified,const RoadState & left = RoadState::NotSpecified, const RoadState& right = RoadState::NotSpecified):
	_left(left),
	_right(right),
	_up(up),
	_down(down),
	_isActive(true)
{
	//empty
}

void NextPosition::AddOpening(const int pos)
{
	intToPath(pos) = NextPosition::RoadState::Opened;
}

void NextPosition::CloseRoad(const int pos)
{
	intToPath(pos) = NextPosition::RoadState::Closed;
}

void NextPosition::RemoveConstraint(const int pos)
{
	intToPath(pos) = NextPosition::RoadState::NotSpecified;
}

bool NextPosition::cardFit(const PathCard & card)
{

	if (!equalStates(card.getUpState(), _up)) {
		return false;
	}
	if (!equalStates(card.getDownState(), _down)) {
		return false;
	}
	if (!equalStates(card.getRightState(), _right)) {
		return false;
	}
	if (!equalStates(card.getLeftState(), _left)) {
		return false;
	}
	return true;
}

bool NextPosition::getState() const
{
	return _isActive;
}


NextPosition::RoadState & NextPosition::intToPath(const int path)
{
	if (path == 1) {
		return _up;
	}
	if (path == 2) {
		return _down;
	}
	if (path == 3) {
		return _left;
	}
	if (path == 4) {
		return _right;
	}
}

bool NextPosition::equalStates(const PathCard::RoadState & roadCard, const RoadState & roadPosition)
{
	/*
	if (roadCard == PathCard::RoadState::Opened && roadPosition == RoadState::Opened) {
		return true;
	}
	if (roadCard == PathCard::RoadState::Closed && roadPosition == RoadState::Closed) {
		return true;
	}
	*/
	
	if (roadPosition == RoadState::Closed && roadCard != PathCard::RoadState::None)
		return false;

	if (roadPosition == RoadState::Opened && roadCard == PathCard::RoadState::None)
		return false;

	return true;
}


