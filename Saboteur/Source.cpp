#include "ActionCard.h"
#include "DwarfCard.h"
#include "PathCard.h"
#include "TreasureCard.h"
#include "Board.h"
#include "Saboteur.h"

#include <iostream>

#include "..\MenuDLL\Menu.h"

#include "ArtPrinter.h"
#include "Instructions.h"
int main() {

	artPrinter();
	
	system("pause");
	Menu m;
	m.addString("Play game");
	m.addString("About");
	m.addString("Quit");
	
	while (true) {
		int i = m.ShowMenu();
		if (i == 0) {
			break;
		}
		if (i == 2) {
			return 0;
		}
		if (i == 1) {
			Instructions instructions;
			instructions.showInfo(std::cout);
			system("pause");
		}
	}
	Board test;

	Saboteur sab;
	sab.startGame();

	
	return 0;
}