#pragma once

#include "PathCard.h"
#include "NextPosition.h"
#include "TreasureCard.h"

#include<map>
#include <array>
#include <unordered_map>


class Board
{
public:
using Position = std::pair<int, int>;
using RoadS = PathCard::RoadState;
using AvailableRoad = NextPosition::RoadState;

const std::array<const Position, 3> _treasurePositions = {Position(-2,8),Position(0,8), Position(2,8)};

public:
	Board();
	friend std::ostream& operator << (std::ostream& os, const Board& board);

public:
	void emplaceCard(const Position& pos,  PathCard& card);
	const std::string getTreasure(const int idx) const;

	bool check(const Position& lastAddedPos) const;

	void deleteCard(const Position& pos);

	bool hasCardsNeighbours(const Position& pos) const;

//creating comparator for Positions
//I think we will delete that thing and will use an unordered map :P
private:
	struct PosComparator {
		bool operator () (const Position& pos1,const  Position& pos2) const;
	};

private: 
	void addPosition(const Position& cardPos, const Position& availablePos);
	bool neighbours(const Position& pos1, const Position& pos2) const;
private:
	std::map<Position, PathCard,PosComparator> _cards;
	std::map<Position, NextPosition,PosComparator> _availablePositions;
	std::array<TreasureCard, 3> _treasures;
	Position _goldPos;
};

