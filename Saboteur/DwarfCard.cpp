#include "DwarfCard.h"

DwarfCard::DwarfCard(DwarfType dwarfType):
	_dwarfType(dwarfType)
{

}

DwarfCard::DwarfType DwarfCard::getDwarfType() const
{
	return _dwarfType;
}

DwarfCard::DwarfCard(const DwarfCard & other) 
{
	*this = other;
}

DwarfCard::DwarfCard(DwarfCard && other) 
{
	*this = std::move(other);
}

DwarfCard & DwarfCard::operator=(const DwarfCard & other)
{
	_dwarfType = other._dwarfType;
	return *this;
}

DwarfCard & DwarfCard::operator=(DwarfCard && other)
{
	*this = other;
	new(&other) Card(Card::CardType::DwarfCard);
	return *this;
}

std::ostream & operator<<(std::ostream & os, const DwarfCard & card)
{
	switch (card.getDwarfType())
	{
	case DwarfCard::DwarfType::DiggerDwarf:
		os << "Digger" << std::endl;
		break;
	case DwarfCard::DwarfType::SaboteurDwarf:
		os << "Saboteur" << std::endl;
		break;
	}

	return os;
}
