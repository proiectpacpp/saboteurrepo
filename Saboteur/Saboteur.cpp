#include "Saboteur.h"
#include <random>
#include <time.h>
#include <string>

#include<iostream>

#include <winsock2.h>	// contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>	// contains newer functions and structures used to retrieve IP addresses

#include <vector>

#pragma comment(lib, "Ws2_32.lib")	//  indicates to the linker that the Ws2_32.lib

#define DEAFULT_ADDRESS "192.168.1.3"

namespace utils {
	std::vector<std::string> split(const char *str, char c = ' ')
	{
		std::vector<std::string> result;

		do
		{
			const char *begin = str;

			while (*str != c && *str)
				str++;

			result.push_back(std::string(begin, str));
		} while (0 != *str++);

		return result;
	}
}


Saboteur::Saboteur()
{
	//init of the _deck
	//-------------------------------------------------------

	PathCardsDeck paths;
	ActionCardsDeck actions;

	

	std::for_each(paths.getCards().begin(), paths.getCards().end(), [this](PathCard& card) {
		aux.push_back(new PathCard(card));
	});

	std::for_each(actions.getDeck().begin(), actions.getDeck().end(), [this](ActionCard& card) {
		aux.push_back(new ActionCard(card));
	});



	//std::default_random_engine randomEngine;
	std::shuffle(aux.begin(), aux.end(),std::default_random_engine(time(NULL)));
	int i = 0;
	for (auto& it : aux) {
		
/*
		if (it->getCardType() == Card::CardType::PathCard) {
			std::cout << *static_cast<PathCard*>(it);
			i++;
		}
		if (it->getCardType() == Card::CardType::ActionCard) {
			std::cout << *static_cast<ActionCard*>(it);
			i ++ ;
		}
		
		std::cout << i;
		*/
		_deck.push(it);

	}


	//-------------------------------------------------------
}

void Saboteur::initPlayers()
{
	/*
	std::cout << "Please enter the number of players: between 3 and 10 : ";
	int temp;
	std::cin >> temp;
	if (temp < 3 || temp>10) {
		throw "The number you entered does not match";
	
	}
	*/
	int temp = 3; //the number of players
	std::vector<DwarfCard> dwarfs;
	generateDwards(temp,dwarfs);

	for (int i = 0; i < temp; i++) {
		std::string name = "Player"+std::to_string(i);
	//	std::cout << "Player " << i + 1 << " name: ";
	//	std::cin >> name;
		_players.push_back(Player(name, dwarfs[i]));
	}
}


void Saboteur::generateDwards(const int number, std::vector<DwarfCard>& aux)
{

	if (number == 3)
	{
		pushingDwarfs(3, 1, aux);
	}
	if (number == 4)
	{
		pushingDwarfs(4, 1, aux);
	}
	if (number == 5)
	{
		pushingDwarfs(4, 2, aux);
	}
	if (number == 6)
	{
		pushingDwarfs(5, 2, aux);
	}
	if (number == 7)
	{
		pushingDwarfs(5, 3, aux);
	}
	if (number == 8)
	{
		pushingDwarfs(6, 3, aux);
	}
	if (number == 9)
	{
		pushingDwarfs(7, 3, aux);
	}
	if (number == 10)
	{
		pushingDwarfs(7, 4, aux);
	}



	std::shuffle(aux.begin(), aux.end(), std::default_random_engine(time(NULL)));
	//aux.erase(aux.begin() + number + 1, aux.end());


}

void Saboteur::pushingDwarfs(const int numberofDiggers, const int numberOfSaboteurs, std::vector<DwarfCard>& container)
{
	for (int i = 0; i < numberofDiggers; i++)
		container.push_back(DwarfCard::DwarfType::DiggerDwarf);
	for (int i = 0; i < numberOfSaboteurs; i++)
		container.push_back(DwarfCard::DwarfType::SaboteurDwarf);

}

void Saboteur::startGame()
{
	
	while (true) {
		try {
			initPlayers();
			break;
		}
		catch (const char* mess) {
			std::cout << mess<<std::endl;
			std::cin.clear();
			std::cin.seekg(std::ios::end);
			
		}
	}
	int currentPlayer = 0;

		

	//partea de conexiune la server.
		//==============================================================================
		
			// *** initialize Winsock ***
	// initialize Winsock before making other Winsock functions calls (only once per application/dll)
			WSADATA wsaData;
			int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);		// initiate use of WS2_32.dll, with version 2.2
			if (iResult != 0)
			{
				throw("WSAStartup failed: %d\n", iResult);
				
			}

			// *** Create a socket ***
			addrinfo *result = NULL, *ptr = NULL, hints;

			ZeroMemory(&hints, sizeof(hints));	// memset to 0
			hints.ai_family = AF_UNSPEC;		// unspecified: either an IPv6 or IPv4
			hints.ai_socktype = SOCK_STREAM;	// stream socket
			hints.ai_protocol = IPPROTO_TCP;	// TCP protocol

#define DEFAULT_PORT "27015"


		// Resolve the server address and port
			iResult = getaddrinfo(DEAFULT_ADDRESS, DEFAULT_PORT, &hints, &result);
			if (iResult != 0)
			{
				throw("getaddrinfo failed: %d\n", iResult);
				//WSACleanup();
			}

			SOCKET ConnectSocket = INVALID_SOCKET;
			// Attempt to connect to the first address returned by the call to getaddrinfo
			ptr = result;

			// Create a SOCKET for connecting to server
			ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

			if (ConnectSocket == INVALID_SOCKET)
			{
				throw("Error at socket(): %ld\n", WSAGetLastError());
			//	freeaddrinfo(result);
			//	WSACleanup();
				
			}

			// *** Connect to server ***
			iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR)
			{
				closesocket(ConnectSocket);
				ConnectSocket = INVALID_SOCKET;
			}

			// Should really try the next address returned by getaddrinfo
			// if the connect call failed
			// But for this simple example we just free the resources
			// returned by getaddrinfo and print an error message

			freeaddrinfo(result);

			if (ConnectSocket == INVALID_SOCKET)
			{
				throw("Unable to connect to server!\n");
			//	WSACleanup();
				
			}
		
		//==============================================================================


		//==============================================================================
			//Clientul isi primeste "numarul de ordine"
#define DEFAULT_BUFLEN 512
			int recvbuflen = DEFAULT_BUFLEN;
			char recvbuf[DEFAULT_BUFLEN];
			const int PLAYERS_NUM = 3;

			//==
			iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			if (iResult > 0)
			{
				//printf("Bytes received: %d\n", iResult);
				//printf("Buffer content: %.*s\n", iResult, recvbuf);
				std::string received;
			//	char temp[DEFAULT_BUFLEN];
			//	sprintf(temp,"%.*s\n\0", iResult, recvbuf);
			//	received = temp;

			}
			else if (iResult == 0)
				printf("Connection closed\n");
			else
				printf("recv failed: %d\n", WSAGetLastError());
			currentPlayer = atoi(recvbuf);
		//==============================================================================
			int currentTurn = 0;
			for (int j = 0; j < 5; j++) {
				_players[currentPlayer].newCard(_deck.top());
				_deck.pop();
			}

	while (true) {
		if (std::none_of(_players.begin(), _players.end(), [](Player& player) {
			return player.getPlayerState();
		})) {
			std::cout << std::endl<< "Saboteur(s) won!" << std::endl;
			break;
		};
		std::string temp; //acest mesaj va fi trimit serverului odata ce pe tabla de joc are loc o "miscare"
		

		system("cls");
		std::cout << _board;

		std::cout << _players[currentPlayer];
		
		std::cout << std::endl;

		if (!_players[currentPlayer].getPlayerState()) {
			//currentPlayer = (currentPlayer + 1) % _players.size();
			//aici vine partea in care trimit mesaj la server ca playerul e inactiv
			//trebuie sa incrementezi current-ul aici  :D
		
		//	temp = 'N';
		}

		if (currentTurn != currentPlayer) {
		//procesare de mesaje primite
			iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			if (iResult > 0)
			{
				printf("Bytes received: %d\n", iResult);
				printf("Buffer content: %.*s\n", iResult, recvbuf);
			}
			else if (iResult == 0)
				printf("Connection closed\n");
			else
				printf("recv failed: %d\n", WSAGetLastError());
			
			std::string received;
			char temporary[DEFAULT_BUFLEN];
			sprintf(temporary, "%.*s\n\0", iResult, recvbuf);
			received = temporary;
			//std::cout << received;
			//system("pause");

			//===========================
			//received e de tip string;
			std::vector<std::string> tokens = utils::split(received.c_str(),'-');
			if (tokens[0] == "E") {
				int x = atoi(tokens[1].c_str());
				int y = atoi(tokens[2].c_str());
				PathCard temporaryCard = PathCard(tokens[3]);
				_board.emplaceCard(Board::Position(x, y), temporaryCard);
			}
			if (tokens[0] == "DP") {
				_players[atoi(tokens[1].c_str())].pickaxeDamage();
			}
			if (tokens[0] == "DL") {
				_players[atoi(tokens[1].c_str())].lightDamage();
			}
			if (tokens[0] == "DC") {
				_players[atoi(tokens[1].c_str())].cartDamage();
			}
			if (tokens[0] == "RP") {
				_players[atoi(tokens[1].c_str())].pickaxeRepair();
			}
			if (tokens[0] == "RL") {
				_players[atoi(tokens[1].c_str())].lightRepair();
			}
			if (tokens[0] == "RC") {
				_players[atoi(tokens[1].c_str())].cartRepair();
			}
			if (tokens[0] == "D") {
				_board.deleteCard(Board::Position(atoi(tokens[1].c_str()), atoi(tokens[2].c_str())));
			}
			if (tokens[0] == "DISCARD") {
				_players[atoi(tokens[1].c_str())].discardAll();
			}
			//--------------------------
		}
		else if (!_players[currentPlayer].getPlayerState() ) {
			std::string nope = "N";
			const char *sendbuf = nope.c_str();
			// Send an initial buffer
			iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
			if (iResult == SOCKET_ERROR)
			{
				printf("send failed: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				system("pause");
			}
		}
		else {

			

			std::cout << "Press 1 to place card on the board" << std::endl;
			std::cout << "Press 2 to use a card on a player" << std::endl;
			std::cout << "Press 3 to discard a card" << std::endl;
			std::cout << "Press 4 for discard all" << std::endl;
			int opt;
			do {
				std::cout << "Your option:";
				std::cin >> opt;
				if (opt < 1 || opt > 4) {
					std::cin.clear();
					std::cin.seekg(std::ios::end);
				}
			} while (opt < 1 || opt > 4);
			
			switch (opt)
			{
			case 1:
				if (!_players[currentPlayer].getOperational()) {
					std::cout << "You cannot place a card on board because you aren't operational" << std::endl;
					system("pause");
					break;
					
				}
				int cardIndex;
				Card* cardToPlace;

				while (true) {
					try {
						std::cout << "Enter the card index:";
						std::cin >> cardIndex;
						cardToPlace = _players[currentPlayer].getCardByIndex(cardIndex);
						break;
					}
					catch (const char* error) {
						std::cout << error << std::endl;
					}
				}
				if (cardToPlace->getCardType() != Card::CardType::PathCard) {
					std::cout << "This is not a path card" << std::endl;
					system("pause");
					break;
				}
				int x, y;
				while (true) {
					try {
						std::cout << "Please enter the x and y coordinates: ";
						std::cin >> x >> y;
						PathCard cardToPathCard = *(static_cast<PathCard*>(cardToPlace));
						char rotate;
						do {
							std::cout << "Do you want to rotate card?[y/n]:";
							std::cin >> rotate;
						} while (rotate != 'y' && rotate != 'n');
						if (rotate == 'y') {
							cardToPathCard.rotate();
						}
						_board.emplaceCard(Board::Position(x, y), cardToPathCard);
						if (_board.check(Board::Position(x, y))) {
							std::cout << std::endl << "DIGGERS WON!" << std::endl;
							return;
						}
						_players[currentPlayer].eraseCard(cardIndex);
						if (!_deck.empty()) {
							_players[currentPlayer].newCard(_deck.top());
							_deck.pop();
						}
						temp = "E-" + std::to_string(x) + '-' + std::to_string(y) + '-' + cardToPathCard.toString();
						//	currentPlayer = (currentPlayer + 1) % _players.size();
						break;
					}
					catch (const char * error) {
						std::cout << error << std::endl;
						char opt2;
						do {
							std::cout << "Do you want to retry?[y/n]:";
							std::cin >> opt2;
						} while (opt2 != 'y' && opt2 != 'n');
						if (opt2 == 'n') {
							break;
						}
					}
				}

				break;
			case 2: {
				while (true) {
					try {
						std::cout << "Enter the card index:";
						std::cin >> cardIndex;
						cardToPlace = _players[currentPlayer].getCardByIndex(cardIndex);
						break;
					}
					catch (const char* error) {
						std::cout << error << std::endl;
					}
				}
				if (cardToPlace->getCardType() != Card::CardType::ActionCard) {
					std::cout << "This is not an action card" << std::endl;
					system("pause");
					break;
				}
				ActionCard actCard = *static_cast<ActionCard*>(cardToPlace);
				if (actCard.getActionType() == ActionCard::ActionType::seeTreasure) {
					while (true) {
						try
						{
							int cardNumber;
							std::cout << "Enter the card number(between 1 and 3 from up to bottom):";
							std::cin >> cardNumber;
							std::cout << _board.getTreasure(cardNumber) << std::endl;
							temp = "N";
							system("pause");

							break;
						}
						catch (const char * error)
						{
							std::cout << error << std::endl;
						}
					}
				}
				if (actCard.getActionType() == ActionCard::ActionType::deletePath) {
					while (true) {
						try {
							std::cout << "Enter the cordinates: ";
							std::cin >> x >> y;
							_board.deleteCard(Board::Position(x, y));
							temp = "D-" + std::to_string(x) + '-'+std::to_string(y);
							break;
						}
						catch (const char* error) {
							std::cout << error << std::endl;;

							do {
								std::cout << "Retry?[y/n]:";
								std::cin >> opt;
							} while (opt != 'y' && opt != 'n');
							if (opt == 'n') {
								break;
							}

						}
					}
				}
				int playerIndex = 0;
				//
				while (actCard.getActionType() != ActionCard::ActionType::seeTreasure && actCard.getActionType() != ActionCard::ActionType::deletePath) {
					try {
						std::string playerName;
						std::cout << "What player do you want to help/sabotage?:";
						std::cin >> playerName;
						if (std::none_of(_players.begin(), _players.end(), [&playerName, &playerIndex](Player& player) {
							if (player.getName() != playerName && player.getPlayerState()) {
								playerIndex++;
							}
							return player.getName() == playerName && player.getPlayerState(); }))
						{
							throw "Name does not matching";
						}
							break;

					}
					catch (const char * error) {
						std::cout << error << std::endl;
					}
				}
				//
				if (actCard.repairOrDestroyOneThing()) {

					switch (actCard.getActionType())
					{
					case ActionCard::ActionType::destoryLight: {
						_players[playerIndex].lightDamage();
						temp = "DL-" + std::to_string(playerIndex);
						break;
					}
					case ActionCard::ActionType::destroyCart: {
						_players[playerIndex].cartDamage();
						temp = "DC-" + std::to_string(playerIndex);
						break;
					}
					case ActionCard::ActionType::destoryPickAxe: {
						_players[playerIndex].pickaxeDamage();
						temp = "DP-" + std::to_string(playerIndex);
						break;
					}
					case ActionCard::ActionType::repairLight: {
						_players[playerIndex].lightRepair();
						temp = "RL-" + std::to_string(playerIndex);
						break;
					}
					case ActionCard::ActionType::repairCart: {
						_players[playerIndex].cartRepair();
						temp = "RC-" + std::to_string(playerIndex);
						break;
					}
					case ActionCard::ActionType::repairPickAxe: {
						_players[playerIndex].pickaxeRepair();
						temp = "RP-" + std::to_string(playerIndex);
						break;
					}
					}

					//RP = repair pickaxe, DC = destroy cart, etc;
				}
				if (actCard.repairTwoThings()) {



					switch (actCard.getActionType())
					{
					case ActionCard::ActionType::repairCartOrLight: {
						std::string opt;
						do {
							std::cout << "Do you want to repair his/her cart or light?[cart/light]:";
							std::cin >> opt;
						} while (opt != "cart" && opt != "light");
						if (opt == "cart") {
							_players[playerIndex].cartRepair();
							temp = "RC-" + std::to_string(playerIndex);
						}
						else {
							_players[playerIndex].lightRepair();
							temp = "RL-" + std::to_string(playerIndex);
						}
					}
																	break;
					case ActionCard::ActionType::repairCartOrPickAxe:

					{
						std::string opt;
						do {
							std::cout << "Do you want to repair his/her cart or pickaxe?[cart/pickaxe]:";
							std::cin >> opt;
						} while (opt != "cart" && opt != "pickaxe");
						if (opt == "cart") {
							_players[playerIndex].cartRepair();
							temp = "RC-" + std::to_string(playerIndex);
						}
						else {
							_players[playerIndex].pickaxeRepair();
							temp = "RP-" + std::to_string(playerIndex);
						}
					}

					break;
					case ActionCard::ActionType::repairPickAxeOrLight:

					{
						std::string opt;
						do {
							std::cout << "Do you want to repair his/her pickaxe or light?[pickaxe/light]:";
							std::cin >> opt;
						} while (opt != "pickaxe" && opt != "light");
						if (opt == "pickaxe") {
							_players[playerIndex].pickaxeRepair();
							temp = "RP-" + std::to_string(playerIndex);
						}
						else {
							_players[playerIndex].lightRepair();
							temp = "RL-" + std::to_string(playerIndex);
						}
					}

					break;
					}

				}
				_players[currentPlayer].eraseCard(cardIndex);
				if (!_deck.empty()) {
					_players[currentPlayer].newCard(_deck.top());
					_deck.pop();
				}
				//	currentPlayer = (currentPlayer + 1) % _players.size();
				break;
			}
			//
			case 3:

				while (true) {
					try {
						std::cout << "Enter the card index:";
						std::cin >> cardIndex;
						//check if the card exists
						if (_deck.empty()) {
							throw "You cannot do that because the deck is empty";
						}
						_players[currentPlayer].getCardByIndex(cardIndex);
						_players[currentPlayer].eraseCard(cardIndex);
						_players[currentPlayer].newCard(_deck.top());
						_deck.pop();
						temp = 'N';
						//	currentPlayer = (currentPlayer + 1)%_players.size();
						break;

					}
					catch (const char * error) {
						std::cout << error << std::endl;
						char playerOption;
						do {
							std::cout << "Do you want to retry?[y/n]:";
							std::cin >> playerOption;
						} while (playerOption != 'y' && playerOption != 'n');
						if (playerOption == 'n') {
							break;
						}
					}
				}

				break;
			case 4:
				_players[currentPlayer].discardAll();
				temp = "DISCARD-" + std::to_string(currentPlayer);
				//	currentPlayer = (currentPlayer + 1) % _players.size();
				break;
			
}
//--
			const char *sendbuf = temp.c_str();
			// Send an initial buffer
			iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
			if (iResult == SOCKET_ERROR)
			{
				printf("send failed: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				system("pause");
			}
			//--
		}
		currentTurn = (currentTurn + 1) % PLAYERS_NUM;
		
	}
}
