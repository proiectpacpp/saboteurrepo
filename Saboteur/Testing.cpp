#include "ActionCard.h"
#include "DwarfCard.h"
#include "PathCard.h"
#include "TreasureCard.h"
#include "Board.h"
#include <iostream>
#include "../MenuDLL/Menu.h"
#include "Game.h"
#include "CommonPack.h"
#include "ActionCardsDeck.h"

#include "Player.h"

int test() {

	Board test;
	test.emplaceCard(Board::Position(0,1),PathCard(PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened));
	test.emplaceCard(Board::Position(-1, 0), PathCard(PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::Opened, PathCard::RoadState::Closed));
	test.emplaceCard(Board::Position(0, -1), PathCard(PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened));
	test.emplaceCard(Board::Position(0, -2), PathCard(PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened));
	test.emplaceCard(Board::Position(0, -3), PathCard(PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::Closed, PathCard::RoadState::Closed));
	test.emplaceCard(Board::Position(-1, -2), PathCard(PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::Opened));
	test.emplaceCard(Board::Position(-1, -1), PathCard(PathCard::RoadState::None, PathCard::RoadState::Opened, PathCard::RoadState::Closed, PathCard::RoadState::Closed));
	test.emplaceCard(Board::Position(-2, -1), PathCard(PathCard::RoadState::None, PathCard::RoadState::None, PathCard::RoadState::Closed, PathCard::RoadState::Closed));
	std::cout << test;
	std::cout << test; 

	CommonPack pack;
	std::vector<Player> vect;
	vect.push_back(Player("test1", test, vect, pack));
	Player player("test", test, vect, pack);
	for (int i = 0; i < 5; i++) {
		player.getNewCard();
	}
	std::cout << player << std::endl;
	player.processCard(new PathCard(PathCard::RoadState::Opened, PathCard::RoadState::Opened, PathCard::RoadState::None, PathCard::RoadState::None));
	
	std::cout << test;

	/*player.processCard(new ActionCard(ActionCard::ActionType::destoryLight));
	std::cout << vect[0] << std::endl;*/
	//Game s;
	//s.initializeGame(6);
	//for (int i = 0; i < 6; i++)
	//	std::cout << s.DwarfVect[i];
	//std::cout << "----------\n";
	//s.initializeGame(6);
	//for (int i = 0; i < 6; i++)
	//	std::cout << s.DwarfVect[i];


	/*
	PathCard pathTest(PathCard::RoadState::Opened, PathCard::RoadState::Closed, PathCard::RoadState::None, PathCard::RoadState::Opened);
	std::cout << pathTest << std::endl;

	pathTest.rotate();
	std::cout << pathTest << std::endl;

	//Testing copy constructor for pathCard

	PathCard pCard2(pathTest);
	std::cout << pCard2 << std::endl;
	//Testing move operator
	std::cout << "Move semantics" << std::endl;
	std::cout<<std::endl;
	PathCard test3 = std::move(pCard2);
	std::cout << test3 << std::endl;
	std::cout << pCard2 << std::endl;

	//don't forget to test NextPosition class 



	// Testing ActionCard class
	ActionCard cardTest(ActionCard::ActionType::deletePath);
	std::cout << cardTest;
	//ActionCard cardTest2(ActionCard::ActionType::repair);
//	std::cout << cardTest2;
	ActionCard cardTest3(ActionCard::ActionType::seeTreasure);
	std::cout << cardTest3;
	std::cout << "Copy test=";
	ActionCard copyTest(cardTest);
	std::cout << copyTest << std::endl;
	
	// Testing TreasureCard class
	TreasureCard treasureTest(TreasureCard::TreasureType::CoalType);
	std::cout << treasureTest;
	TreasureCard treasureTest2(TreasureCard::TreasureType::GoldType);
	std::cout << treasureTest2;

	// Testing copy constructor for TreasureCard
	std::cout << "Copy test: ";
	TreasureCard cTest(treasureTest);
	std::cout << cTest << std::endl;

	// Testing move operator
	std::cout << "Move semantics" << std::endl;
	TreasureCard mTest = std::move(cTest);
	std::cout << mTest << std::endl;
	std::cout << cTest << std::endl;

	// Testing DwarfCard class
	DwarfCard dwarfTest(DwarfCard::DwarfType::DiggerDwarf);
	std::cout << dwarfTest;
	DwarfCard dwarfTest2(DwarfCard::DwarfType::SaboteurDwarf);
	std::cout << dwarfTest2;

	// Testing copy constructor for DwarfCard
	std::cout << "Copy test: ";
	DwarfCard dTest(dwarfTest);
	std::cout << dTest << std::endl;

	// Testing move operator for DwarfCard
	std::cout << "Move semantics" << std::endl;
	DwarfCard dwTest = std::move(dTest);
	std::cout << dwTest << std::endl;
	std::cout << dTest << std::endl;
	*/
	return 0;
}

int main() {
	Menu myMenu;
	myMenu.addString("Information");
	myMenu.addString("Start game");
	myMenu.addString("Quit");

	while (true) {
		system("cls");
		char opt;
		std::cout << "w for up, s for down" << std::endl;
		myMenu.ShowMenu();
		opt = getch();
		if (opt == 'w') {
			myMenu.goUp();
		}
		if (opt == 's') {
			myMenu.goDown();
		}
		if (opt == ((char)13)) {


		}

	}

	return 0;
}