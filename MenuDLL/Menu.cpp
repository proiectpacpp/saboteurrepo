#include "Menu.h"


Menu::Menu()
{
}

Menu::~Menu()
{
}

void Menu::addString(std::string string)
{
	options.push_back(string);
}

void Menu::goDown()
{

	current = (++current) % options.size();
}

void Menu::goUp()
{
	if (current == 0) {
		current = options.size() - 1;
	}
	else {
		current--;
	}
}

char Menu::getOption()
{
	return current;
}

int Menu::ShowMenu()
{
		system("cls");
		char opt;
		while (true) {
			system("cls");
			std::cout << *this;
			opt = _getch();
			if (opt == 's') {
				goDown();
			}
			if (opt == 'w') {
				goUp();
			}
			if (opt == (char)13) {
				return current;
			}
			
	}
}

std::ostream & operator<<(std::ostream & os, const Menu & myMenu)
{

	for (int i = 0; i < myMenu.options.size(); i++) {
		if (i == myMenu.current) {
			std::cout << ">";
		}
		else
		{
			std::cout << " ";
		}
		os << myMenu.options[i] << std::endl;
	}
	return os;
}
