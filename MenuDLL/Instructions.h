#pragma once
#pragma once

#include <iostream>

class Instructions
{
public:
	std::ostream &showInfo(std::ostream &os) const
	{
		os << "	SABOTEUR\n"
			"	IDEA OF THE GAME\n"
			"  The players take on the part of dwarves. They can be gold-diggers, workig their "
			"tunnels ever deeper into the mountain in search of treasure, or saboteurs,"
			"trying to put obstacles into the diggers' path. The members of each group should\n"
			"support each other once they figure out"
			" who is on their side. If the gold-diggers manage to "
			"create a path to the\ntreasure, they are rewarded with nuggets of gold."
			"If the gold-diggers fail, the saboteurs reap the reward. "
			"After three\nrounds, the player with most nuggets wins.\n"
			"	HOW TO PLAY\n"
			"	PATH CARD\n"
			"  After the cards are dealt, the first player must play a card. This means either add a path "
			"to the maze, put down an\naction card in front of a player or pass, by putting one card face"
			" down on the discard pile and taking another one from the deck if possible. Bit by bit, the"
			" path cards form a way from the start card to the goal cards. A path card must be\nalways put"
			" next to another path card if it fit but it can never be played crosswise.\n"
			"	ACTION CARD\n"
			"  Action cards are played by putting them face-up in front of a player. Action cards can hinder"
			"(destroy Cart, Light or\nPickaxe) or help(repair something broken or see one of the goal cards)"
			". The action cards can also delete a path from themaze. This move can also help or hinder the players.\n"
			"	END OF A ROUND\n"
			"  When a player plays a path card and reaches a goal card, the goal card is turned over. If it "
			"is a gold card, the round\nis over and the gold-diggers win. If it is a coal card, the round "
			"continues. The round is also over if the deck is used up and each player has no playable cards "
			"left in hand. In this case, the sabs win.\n" "  If the gold-diggers have won the round, the nugget "
			"cards are dealt to them. If not, the sabs take the nuggets gold.\n  A new round begins. The start "
			"card and goal card are put on the table just like at the beginning of the game.\n"
			"	END OF THE GAME\n"
			"  The game ends after the third round. The player with the most nuggets wins the game. If there is"
			" more than one player with the same nuber of nuggets, they are tied in first place.\n";
		return os;
	}
};
