#pragma once
#ifdef MENU_EXPORTS
#define MENU_API __declspec(dllexport)
#else
#define MENU_API __declspec(dllimport)
#endif

#include <iostream>
#include <vector>
#include <conio.h>
#include <string>
#include <algorithm>

class MENU_API Menu
{
public:
	Menu();
	~Menu();
	std::vector<std::string> options;
	int current = 0;
	void addString(std::string string);
	void goDown();

	void goUp();

	friend std::ostream & operator<<(std::ostream & os, const Menu & myMenu);
	char getOption();
	int ShowMenu();
};